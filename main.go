package digikey

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

// Client holds the HTTP client and token information.
type Client struct {
	httpClient *http.Client
	Token      *Token
	clientID   string
	clientSecret string
	domain string
}

// Token holds the OAuth token details.
type Token struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
	Expiry      time.Time
}


// NewClient creates a new API client and fetches the OAuth token.
func NewClient(clientID, clientSecret string, production bool) (*Client, error) {
	client := &Client{
		httpClient: &http.Client{},
	}

	subdomain := ""
	if production {
		subdomain = "api"
	} else {
		subdomain = "sandbox-api"
	}

	client.domain = "https://" + subdomain + ".digikey.com"

	// Fetch the token
	token, err := client.authenticate(clientID, clientSecret)
	if err != nil {
		return nil, err
	}

	client.Token = token
	return client, nil
}

// authenticate handles the OAuth authentication process.
func (c *Client) authenticate(clientID, clientSecret string) (*Token, error) {
	formData := url.Values{
		"grant_type":    []string{"client_credentials"},
		"client_id":     []string{clientID},
		"client_secret": []string{clientSecret},
	}

	req, err := http.NewRequest("POST", client.domain + "/v1/oauth2/token", strings.NewReader(formData.Encode()))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		fmt.Printf("Failed to authenticate: %s\nResponse body: %s\n", resp.Status, string(body))
		return nil, fmt.Errorf("authentication failed, status code: %d", resp.StatusCode)
	}

	var token Token
	if err := json.Unmarshal(body, &token); err != nil {
		return nil, err
	}

	token.Expiry = time.Now().Add(time.Duration(token.ExpiresIn) * time.Second)

	return &token, nil
}

// MakeRequest makes an API request using the token for authentication.
func (c *Client) MakeRequest(endpoint string) ([]byte, error) {
	req, err := http.NewRequest("GET", apiBaseUrl+endpoint, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.Token.AccessToken))
	req.Header.Add("X-DIGIKEY-Client-Id", client.clientID) // Include the X-DIGIKEY-Client-Id header

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

/*
func main() {
	client, err := NewClient(clientID, clientSecret)
	if err != nil {
		fmt.Println("Failed to create client:", err)
		return
	}

	response, err := client.searchProducts(SearchRequest{
		Keywords: "capacitor",
		Limit:    10,
	})
	if err != nil {
		fmt.Println("Failed to search products: ", err)
		return
	}

	fmt.Println(response)
}
*/
