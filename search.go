package digikey

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

// search types
type SearchRequest struct {
	Keywords             string        `json:"Keywords"`
	Limit                int           `json:"Limit"`
	Offset               int           `json:"Offset"`
	FilterOptionsRequest FilterOptions `json:"FilterOptionsRequest"`
	SortOptions          SortOptions   `json:"SortOptions"`
}

type FilterOptions struct {
	ManufacturerFilter       []FilterID             `json:"ManufacturerFilter"`
	CategoryFilter           []FilterID             `json:"CategoryFilter"`
	StatusFilter             []FilterID             `json:"StatusFilter"`
	PackagingFilter          []FilterID             `json:"PackagingFilter"`
	MarketPlaceFilter        string                 `json:"MarketPlaceFilter"`
	SeriesFilter             []FilterID             `json:"SeriesFilter"`
	MinimumQuantityAvailable int                    `json:"MinimumQuantityAvailable"`
	ParameterFilterRequest   ParameterFilterRequest `json:"ParameterFilterRequest"`
	SearchOptions            []string               `json:"SearchOptions"`
}

type FilterID struct {
	Id string `json:"Id"`
}
type FilterItem struct {
	Id           int    `json:"Id"`
	Value        string `json:"Value"`
	ProductCount int    `json:"ProductCount"`
}

type AppliedFilter struct {
	Id       int    `json:"Id"`
	Text     string `json:"Text"`
	Priority int    `json:"Priority"`
}

type ParameterFilterRequest struct {
	CategoryFilter   FilterID          `json:"CategoryFilter"`
	ParameterFilters []ParameterFilter `json:"ParameterFilters"`
}
type FilterValue struct {
	ProductCount    int    `json:"ProductCount"`
	ValueId         string `json:"ValueId"`
	ValueName       string `json:"ValueName"`
	RangeFilterType string `json:"RangeFilterType"`
}
type TopCategory struct {
	RootCategory Category `json:"RootCategory"`
	Category     Category `json:"Category"`
	Score        float64  `json:"Score"`
	ImageUrl     string   `json:"ImageUrl"`
}

type ParametricFilterItem struct {
	Category      FilterItem    `json:"Category"`
	ParameterType string        `json:"ParameterType"`
	ParameterId   int           `json:"ParameterId"`
	ParameterName string        `json:"ParameterName"`
	FilterValues  []FilterValue `json:"FilterValues"`
}
type ParameterFilter struct {
	ParameterId  int        `json:"ParameterId"`
	FilterValues []FilterID `json:"FilterValues"`
}

type SortOptions struct {
	Field     string `json:"Field"`
	SortOrder string `json:"SortOrder"`
}

type Manufacturer struct {
	Id   int    `json:"Id"`
	Name string `json:"Name"`
}

type Description struct {
	ProductionDescription string `json:"ProductionDescription"`
	DetailedDescription   string `json:"DetailedDescription"`
}

// return types
type SearchResult struct {
	Products                    []Product             `json:"Products"`
	ProductsCount               int                   `json:"ProductsCount"`
	ExactMatches                []Product             `json:"ExactMatches"`
	FilterOptions               FilterOptionsResponse `json:"FilterOptions"`
	SearchLocaleUsed            LocaleInfo            `json:"SearchLocaleUsed"`
	AppliedParametricFiltersDto []AppliedFilter       `json:"AppliedParametricFiltersDto"`
}

type Product struct {
	Description                Description    `json:"Description"`
	Manufacturer               Manufacturer   `json:"Manufacturer"`
	ManufacturerProductNumber  string         `json:"ManufacturerProductNumber"`
	UnitPrice                  float64        `json:"UnitPrice"`
	ProductUrl                 string         `json:"ProductUrl"`
	DatasheetUrl               string         `json:"DatasheetUrl"`
	PhotoUrl                   string         `json:"PhotoUrl"`
	ProductVariations          []Variation    `json:"ProductVariations"`
	QuantityAvailable          int            `json:"QuantityAvailable"`
	ProductStatus              Status         `json:"ProductStatus"`
	BackOrderNotAllowed        bool           `json:"BackOrderNotAllowed"`
	NormallyStocking           bool           `json:"NormallyStocking"`
	Discontinued               bool           `json:"Discontinued"`
	EndOfLife                  bool           `json:"EndOfLife"`
	Ncnr                       bool           `json:"Ncnr"`
	PrimaryVideoUrl            *string        `json:"PrimaryVideoUrl"`
	Parameters                 []Parameter    `json:"Parameters"`
	BaseProductNumber          ProductNumber  `json:"BaseProductNumber"`
	Category                   Category       `json:"Category"`
	DateLastBuyChance          *time.Time     `json:"DateLastBuyChance"`
	ManufacturerLeadWeeks      string         `json:"ManufacturerLeadWeeks"`
	ManufacturerPublicQuantity int            `json:"ManufacturerPublicQuantity"`
	Series                     Series         `json:"Series"`
	ShippingInfo               *ShippingInfo  `json:"ShippingInfo"`
	Classifications            Classification `json:"Classifications"`
}

type FilterOptionsResponse struct {
	Manufacturers      []FilterItem           `json:"Manufacturers"`
	Packaging          []FilterItem           `json:"Packaging"`
	Status             []FilterItem           `json:"Status"`
	Series             []FilterItem           `json:"Series"`
	ParametricFilters  []ParametricFilterItem `json:"ParametricFilters"`
	TopCategories      []TopCategory          `json:"TopCategories"`
	MarketPlaceFilters []string               `json:"MarketPlaceFilters"`
}

type Variation struct {
	DigiKeyProductNumber            string       `json:"DigiKeyProductNumber"`
	PackageType                     Type         `json:"PackageType"`
	StandardPricing                 []PriceBreak `json:"StandardPricing"`
	MyPricing                       []PriceBreak `json:"MyPricing"`
	MarketPlace                     bool         `json:"MarketPlace"`
	TariffActive                    bool         `json:"TariffActive"`
	Supplier                        Manufacturer `json:"Supplier"`
	QuantityAvailableforPackageType int          `json:"QuantityAvailableforPackageType"`
	MaxQuantityForDistribution      int          `json:"MaxQuantityForDistribution"`
	MinimumOrderQuantity            int          `json:"MinimumOrderQuantity"`
	StandardPackage                 int          `json:"StandardPackage"`
	DigiReelFee                     float64      `json:"DigiReelFee"`
}

type Status struct {
	Id     int    `json:"Id"`
	Status string `json:"Status"`
}

type Parameter struct {
	ParameterId   int    `json:"ParameterId"`
	ParameterText string `json:"ParameterText"`
	ParameterType string `json:"ParameterType"`
	ValueId       string `json:"ValueId"`
	ValueText     string `json:"ValueText"`
}

type ProductNumber struct {
	Id   int    `json:"Id"`
	Name string `json:"Name"`
}

type Category struct {
	CategoryId      int        `json:"CategoryId"`
	ParentId        int        `json:"ParentId"`
	Name            string     `json:"Name"`
	ProductCount    int        `json:"ProductCount"`
	NewProductCount int        `json:"NewProductCount"`
	ImageUrl        string     `json:"ImageUrl"`
	SeoDescription  string     `json:"SeoDescription"`
	ChildCategories []Category `json:"ChildCategories"`
}

type Series struct {
	Id   int    `json:"Id"`
	Name string `json:"Name"`
}

type ShippingInfo struct {
	// Example fields, adjust as necessary
	Method string  `json:"Method"`
	Cost   float64 `json:"Cost"`
}

type Classification struct {
	ReachStatus              string `json:"ReachStatus"`
	RohsStatus               string `json:"RohsStatus"`
	MoistureSensitivityLevel string `json:"MoistureSensitivityLevel"`
	ExportControlClassNumber string `json:"ExportControlClassNumber"`
	HtsusCode                string `json:"HtsusCode"`
}

// LocaleInfo represents locale information used during the search.
type LocaleInfo struct {
	Site     string `json:"Site"`
	Language string `json:"Language"`
	Currency string `json:"Currency"`
}

// Type represents a type with an ID and Name.
type Type struct {
	Id   int    `json:"Id"`
	Name string `json:"Name"`
}

// PriceBreak represents pricing information based on quantity.
type PriceBreak struct {
	BreakQuantity int     `json:"BreakQuantity"`
	UnitPrice     float64 `json:"UnitPrice"`
	TotalPrice    float64 `json:"TotalPrice"`
}

/// functions

func (client *Client) SearchProducts(requestData SearchRequest) (*SearchResult, error) {
	jsonData, err := json.Marshal(requestData)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", client.domain+"/products/v4/search/keyword", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", client.Token.AccessToken))
	req.Header.Add("X-DIGIKEY-Client-Id", client.clientID) // Include the X-DIGIKEY-Client-Id header

	resp, err := client.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var result SearchResult
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return nil, err
	}

	return &result, nil
}
